# Guide de fabrication d'une éolienne 
Bonjour à toustes et bienvenue sur notre page!!  
Sur ce site vous pourrez apprendre à fabriquer une &#x2728; éolienne &#x2728; chez vous avec des matériaux simples à trouver.   

## Qui est derrière ce site ?   
Notre équipe se compose de 5 étudiant.e.s en dernière année de bachelier en bio-ingénierie: &#x1F9EA;  
    
- Camille   
- Celiane   
- Loic   
- Irini   
- Wissam    

Depuis septembre, nous nous sommes informés sur le sujet des terres rares, tout en travaillant sur la conception d'un portfolio et d'une maquette.  
Notre objectif depuis le départ étant de partager nos connaissances avec vous, public du festival des sciences de l'ULB.   

## Comment fonctionne une éolienne ?    
Avant de plonger dans la construction de votre mini éolienne, remontons un peu le temps...   
Les premiers moulins remontent il y a plusieurs siècles, ceux-ci utilisaient le pouvoir du vent pour faire tourner des mécanismes mécaniques permettant par exemple de pomper de l'eau ou broyer des céréales.        
Les éoliennes émergent en 1970, ce sont des aérogénérateurs qui exploitent la force du vent pour convertir l’énergie en électricité, contribuant ainsi à nos réseaux électriques. 
Leur importance réside dans leur capacité à générer cette électricité de manière propre, sans émissions de gaz à effet de serre, et avec des ressources pratiquement illimitées.       
Lorsque le vent souffle, il met les pales en mouvement! Celles ci sont connectées à un rotor qui tourne à l'aide de la force du vent.    
Le rotor est lui même relié à un générateur qui convertit ensuite l'énergie mécanique produite par le mouvement du rotor en énergie électrique.     
Cette électricité est ensuite acheminée via des câbles vers les réseaux électriques pour être distribuée à la population.    

## Liste de matériel nécessaire   
Avertissement: Nous avons utilisé des imprimantes 3D pour faire nos éoliennes par choix esthétique mais ce n'est absolument pas obligatoire!  
Ici nous allons donc vous présenter une autre façon de faire, de manière à ce que vous aussi puissez profiter d'une éolienne "fait maison"      
   
<label onclick="toggleCheckbox('dc_motor')"><input type="checkbox" id="dc_motor"> Moteur DC</label><br>
<label onclick="toggleCheckbox('small_lamp')"><input type="checkbox" id="small_lamp"> Petite lampe (max 2 volts)</label><br>
<label onclick="toggleCheckbox('regular_cables')"><input type="checkbox" id="regular_cables"> Câbles classiques</label><br>
<label onclick="toggleCheckbox('light_material')"><input type="checkbox" id="light_material"> Matériel léger pour les pâles (exemple: cannettes , carton, liège)</label><br>
    
## C'est quoi un moteur DC &#x2753; &#x2753;  
La première chose à savoir c'est ce que veut dire moteur DC &#x1F914;  
DC c'est pour "Direct current" ou courant direct en français car le moteur va recevoir de l'électricité (courant) et peut la convertir en énergie mécanique!  
Mais nous on ne veut pas créer un ventilateur, on veut créer une éolienne.   
Pas de problème, le moteur DC peut aussi faire le chemin inverse donc les pâles tournent avec la force du vent ce qui crée de l'énergie mécanique ensuite convertie en électricité &#x1F62E;    
On parle alors de "générateur".   
   
### De quoi est composé un moteur &#x2753; &#x2753;
Le moteur est divisé en deux parties: Stator et Rotor  

Le stator ne bouge pas, et il comporte un noyau magnétique enroulé de fils électriques. 
Quand le courant y passe, il crée un champ magnétique. 

Le rotor est la partie qui tourne, elle est mobile et contient des aimants permanents &#x1F9F2; (comportant des terres rares)
 ou des bobines électromagnétiques.   
C'est le champ magnétique du stator qui mets le rotor en mouvement. 

![rotor](images/Electric_motor.gif)
[*Image du moteur DC][1]


## Comment procéder à la fabrication de l'éolienne ?     
### Première étape 1️⃣    
Pour commencer, on va faire un peu de bricolage &#128296;  
Pour créer les pâles, vous aurez besoin d'un matériel très léger mais rigide!   
Nous vous conseillons d'utiliser soit du carton, soit du plastique de bouteille :))    
<ul>
        <li>La première chose à faire est de couper plusieurs disques (~9/10) de rayons 3,5cm et de les empiler pour créer le support sur lequel les pales viendront s'attacher</li>
        <li>Couper 3 rectangles de 3,5cm de largeur et 15,5cm de longueur</li>
        <li>Couper un demi cercle de rayon 3,5cm à un côté du rectangle</li>
    </ul>     

### Deuxième étape &#x0032;&#xFE0F;&#x20E3;     
Maintenant qu'on a tout découpé pour la partie mobile de l'éolienne, on va pouvoir coller!     
<ul>
        <li>Superposer les disques et les coller ensemble pour créer le support des pâles</li>
        <li>Si besoin esthétique, coller une bandelette de papier sur le contour du support des pales</li>
        <li>Coller les pales mais <span style="color: red;">⚠️ ATTENTION ⚠️</span> à les positionner en diagonale donc un côté du demi cercle en bas du support et l'autre en haut</li>
    </ul>

![dessin](images/dessin.jpg)
Remarque: Essayer d'avoir un angle de 120° entre chaque pâle
### Troisième étape &#x0033;&#xFE0F;&#x20E3;   

Il est temps de passer au mat (ou au pylone) !      
Il y a plusieurs façons de le réaliser, mais la méthode la plus courante implique l'utilisation d'une bouteille. &#x1F37C;   
   
<ul>
    <li>Remplir la bouteille de cailloux, eau, sable... n'importe quelle matière qui rende la bouteille plus lourde</li>
    <li>Faire un trou de même diamètre que votre moteur DC</li>
    <li>Faire un trou de l'autre côté mais pas plus grand qu'un centimètre 
    <li>Coller le moteur DC dans le grand trou pour qu'il soit bien stable</li>
</ul>

## Comment faire le circuit électrique ? 
Une fois le montage de l'éolienne terminé, il est temps de passer à l'étape du montage électrique. 	&#x26A1;   
Ne vous inquiétez pas, cette phase n'est pas très complexe! 
Vous aurez besoin de vos 2 fils électriques et de votre petite Led 	&#x1F4A1;  
![led](images/LEDs.jpg)
[*image Led][2]   
Pour information, vous remarquerez que votre led a une tige plus longue que l'autre, c'est le côté + et celle plus courte est le côté -   
Vous pouvez facilement connecter ces tiges aux sorties d'alimentation du moteur DC à l'aide d'un câble.    
Connectez simplement une tige à une sortie et l'autre tige à la deuxième sortie.     
Rappel, Ceci sont les sorties d'alimentation:    
![sorties DC](images/sorties%20DC.jpg)   
Selon le sens du circuit, votre éolienne tournera dans un sens ou dans l'autre! 
## Plus qu'à souffler &#128521;
A l'aide d'un ventilateur ou d'un sèche cheveux, vous pouvez faire tourner les pâles de l'éolienne et donc former de l'énergie mécanique qui sera transformée en énergie électrique pour allumer votre petite lampe &#x1F4A1;  

## Merci 

Nous espérons que cette guide vous aura été utile pour créer votre propre petite éolienne maison et que vous avez apprécié rentrer dans le monde des ingénieur.e.s un bref instant.    
   
La team expo vous remercie &#x1F496;
















[1]: https://commons.wikimedia.org/w/index.php?curid=4087521 "Par Abnormaal — Travail personnel, CC BY-SA 3.0, source: Wikimedia Commons"
[2]: https://commons.wikimedia.org/w/index.php?curid=227264 "Par Afrank99 — Travail personnel, CC BY-SA 2.0, source: Wikimedia Commons"


